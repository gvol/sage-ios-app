//
//  AlephCell.h
//  iSage
//
//  Created by Ivan Andrus on 4/29/11.
//  Copyright 2011 Ivan Andrus. All rights reserved.
//
/*
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
*/
 
 
#import <CoreData/CoreData.h>

typedef enum {
    // Final condition
    AlephCellStatusHasOutput = 0, // No work to be done
    AlephCellStatusSyntaxError,
    // Intermediate conditions
    AlephCellStatusHasInput,
    AlephCellStatusHasAlephID,    // currently means the same as isWorking
    // Shouldn't be here
    AlephCellStatusUninitialized,
    // Error conditions
    AlephCellStatusJSONParseError, // no longer used
    AlephCellStatusNetworkError,   // no longer used
} AlephCellStatus;


@interface AlephCell :  NSManagedObject
{
    BOOL isWorking;
    int numRequests;
}

- (NSDictionary*)needsRequest:(BOOL)retryErrors;

- (void)clearError;
- (void)setError:(BOOL)wasNetworkError;
- (BOOL)updateWithResponse:(id)response;
- (BOOL)isWorking;
- (BOOL)canDisplayOutput;
- (NSString*)alephURL;

@property (nonatomic, strong) NSNumber * status;
@property (nonatomic, strong) NSString * alephid;
@property (nonatomic, strong) NSDate * creation_time;
@property (nonatomic, strong) NSString * input;
@property (nonatomic, strong) NSString * output;
@property (nonatomic, strong) NSString * html_output;
@property (nonatomic, strong) NSString * language;
@property (nonatomic, strong) NSData * image;
@property (nonatomic) int numRequests;
@property (nonatomic) BOOL isWorking;

@end
