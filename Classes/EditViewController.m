//
//  EditViewController.m
//  iSage
//
//  Created by Ivan Andrus on 4/29/11.
//  Copyright 2011 Ivan Andrus. All rights reserved.
//
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

#import "EditViewController.h"
#import "RootViewController.h"
#import "AlephCell.h"
#import "JSON+iSage.h"
#import "NSData+Base64.h"


@implementation EditViewController

//@synthesize selectedObject;
@synthesize delegate;
@synthesize keyboardAccessory;
@synthesize scrollView;
@synthesize inputView;
@synthesize outputView;
@synthesize FullScreenOutput=_FullScreenOutput;
@synthesize Loaded;
@synthesize shouldLoad=_shouldLoad;
@synthesize actionSheet=_actionSheet;
@synthesize longPress=_longPress;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    UIBarButtonItem *btnShare = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareButtonPressed)];
    UIBarButtonItem *evalButton = [[UIBarButtonItem alloc] initWithTitle:@"Evaluate" style:UIBarButtonItemStyleBordered target:self action:@selector(evalButtonPressed)];
    
    self.navigationItem.rightBarButtonItems = @[btnShare, evalButton];
    
    self.FullScreenOutput = NO;
    self.Loaded = NO;
    self.shouldLoad = NO;
    self.actionSheet = nil;
    self.evaluateAndShareSegmentedController.selectedSegmentIndex=UISegmentedControlNoSegment;
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *HTMLFile = [bundle pathForResource:@"template" ofType:@"html"];
    NSString *HTMLData = [NSString stringWithContentsOfFile:HTMLFile
                                               usedEncoding:nil
                                                      error:nil];

    NSString *oldOutput = [self.selectedObject valueForKey:@"html_output"];
    // We don't want (null) showing up...
    if ( !oldOutput && [self.selectedObject valueForKey:@"output"]) {
        // data migration
        oldOutput = [NSString stringWithFormat:@"<pre>%@</pre>",
                     [self.selectedObject valueForKey:@"output"]];
    }
    // If it was working but the app was quit in the middle
    if ([[self.selectedObject valueForKey:@"status"] intValue] == AlephCellStatusHasAlephID ) {
        [self.selectedObject setValue:[NSNumber numberWithInt:AlephCellStatusHasInput]
                          forKey:@"status"];
    }
    if ( !oldOutput || ([self.selectedObject isKindOfClass:[AlephCell class]]
                        && ![(AlephCell*)self.selectedObject canDisplayOutput]) ) {
        oldOutput = @"<span style=\"color:grey;\">There is no output yet.</span>";
    }

    NSString* alephURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"sageServer"];
    if ( [alephURL isEqual:@"CustomURL"] ) {
        alephURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"customSageServer"];
    }

    [outputView loadHTMLString:[NSString stringWithFormat:HTMLData, oldOutput]
                       baseURL:[NSURL URLWithString:alephURL]];
    
    [self registerForKeyboardNotifications];


    self.navigationItem.title = [self.selectedObject valueForKey:@"language"];

    // Long press ((meant for eval button) but works anywhere in the
    // navigation bar) to select which language.
    self.longPress = [[UILongPressGestureRecognizer alloc]
                 initWithTarget:self
                 action:@selector(selectLanguage:)];
    self.longPress.minimumPressDuration = 1.0;

    // Swipe and increase
    UISwipeGestureRecognizer *doubleSwipeRight =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swipe:)];
    doubleSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    doubleSwipeRight.numberOfTouchesRequired = 2;
    UISwipeGestureRecognizer *doubleSwipeLeft =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swipe:)];
    doubleSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    doubleSwipeLeft.numberOfTouchesRequired = 2;

    UISwipeGestureRecognizer *doubleSwipeUp =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swipe:)];
    doubleSwipeUp.direction = UISwipeGestureRecognizerDirectionLeft;
    doubleSwipeUp.numberOfTouchesRequired = 2;

    UISwipeGestureRecognizer *doubleSwipeDown =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swipe:)];
    doubleSwipeDown.direction = UISwipeGestureRecognizerDirectionLeft;
    doubleSwipeDown.numberOfTouchesRequired = 2;

    [inputView addGestureRecognizer:doubleSwipeRight];
    [inputView addGestureRecognizer:doubleSwipeLeft];
    [inputView addGestureRecognizer:doubleSwipeUp];
    [inputView addGestureRecognizer:doubleSwipeDown];

    // UIWebView does not play well with gesture recognizers.
    UITapGestureRecognizer * twoTaps = [[UITapGestureRecognizer alloc]
                                        initWithTarget:self
                                        action:@selector(twoTaps:)];
    twoTaps.numberOfTapsRequired = 2;
    twoTaps.numberOfTouchesRequired = 1;
    [outputView addGestureRecognizer:twoTaps];
    twoTaps.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Start editing right away if there is no output
    if ( [self.selectedObject valueForKey:@"output"] == nil ) {
        [inputView becomeFirstResponder];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addGestureRecognizer:self.longPress];

    inputView.text = [self.selectedObject valueForKey:@"input"];
    [self updateOutput];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar removeGestureRecognizer:self.longPress];

    [self saveOutput:[outputView stringByEvaluatingJavaScriptFromString:@"isage_get_status()"]];
}

#pragma mark -
#pragma mark Stuff
- (void) setSelectedObject:(NSManagedObject *)selectedObject
{
    [self saveOutput:[outputView stringByEvaluatingJavaScriptFromString:@"isage_get_status()"]];
    _selectedObject=selectedObject;
    
    
}


-(void)updateView
{
    
    self.FullScreenOutput = NO;
    self.Loaded = NO;
    self.shouldLoad = NO;
    self.actionSheet = nil;
    self.evaluateAndShareSegmentedController.selectedSegmentIndex=UISegmentedControlNoSegment;
    NSString *HTMLFile = [[NSBundle mainBundle] pathForResource:@"template" ofType:@"html"];
    NSString *HTMLData = [NSString stringWithContentsOfFile:HTMLFile
                                               usedEncoding:nil
                                                      error:nil];
    
    NSString *oldOutput = [self.selectedObject valueForKey:@"html_output"];
    // We don't want (null) showing up...
    if ( !oldOutput && [self.selectedObject valueForKey:@"output"]) {
        // data migration
        oldOutput = [NSString stringWithFormat:@"<pre>%@</pre>",
                     [self.selectedObject valueForKey:@"output"]];
    }
    // If it was working but the app was quit in the middle
    if ([[self.selectedObject valueForKey:@"status"] intValue] == AlephCellStatusHasAlephID ) {
        [self.selectedObject setValue:[NSNumber numberWithInt:AlephCellStatusHasInput]
                          forKey:@"status"];
    }
    if ( !oldOutput || ([self.selectedObject isKindOfClass:[AlephCell class]]
                        && ![(AlephCell*)self.selectedObject canDisplayOutput]) ) {
        oldOutput = @"<span style=\"color:grey;\">There is no output yet.</span>";
    }
    
    NSString* alephURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"sageServer"];
    if ( [alephURL isEqual:@"CustomURL"] ) {
        alephURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"customSageServer"];
    }
    
    [outputView loadHTMLString:[NSString stringWithFormat:HTMLData, oldOutput]
                       baseURL:[NSURL URLWithString:alephURL]];
    
    [self registerForKeyboardNotifications];
    
    
    self.navigationItem.title = [self.selectedObject valueForKey:@"language"];
    
    inputView.text = [self.selectedObject valueForKey:@"input"];
    [self updateOutput];
    


}
- (void)saveOutput:(NSString*)status {

    // NSLog(@"Updating status to %@",status);
    if ( ![self.selectedObject isKindOfClass:[AlephCell class]] ) {
        NSLog(@"Something really weird happened when evaluating");
        return;
    }
    AlephCell* cell = (AlephCell*)self.selectedObject;

    // Update status
    // If we don't return in this block then it's assumed we want to save such output as there is
    if ( [status isEqualToString:@"busy"] ) {
        //cell.isWorking = YES;
        cell.status = [NSNumber numberWithInt:AlephCellStatusHasAlephID];
        cell.isWorking = YES;
        return;
    }
    cell.isWorking = NO;
    if ( [status isEqualToString:@"input"] ) {
        cell.status = [NSNumber numberWithInt:AlephCellStatusHasInput];
        return;

    } else if ( [status isEqualToString:@"syntax_error"] ) {
        cell.status = [NSNumber numberWithInt:AlephCellStatusSyntaxError];

    } else if ( [status isEqualToString:@"output"] ) {
        cell.status = [NSNumber numberWithInt:AlephCellStatusHasOutput];

    }

    NSString * out = [outputView stringByEvaluatingJavaScriptFromString:@"isageGetOutputForStorage()"];
    if ( ! [out isEqualToString:@""] ) {

        cell.html_output = out;
        NSRange r1 = [out rangeOfString:@"data:image/png;base64,"];
        if (r1.location == NSNotFound) {
            cell.image = nil;
        } else {

            NSString* sub = [out substringFromIndex:NSMaxRange(r1)];
            NSRange r2 = [sub rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];

            // For some reason scaling it myself makes it look much nicer
            UIImage * image = [UIImage imageWithData:[NSData dataFromBase64String:[sub substringToIndex:r2.location]]];
            CGSize size = CGSizeMake(70.f, 70.0f);
            UIGraphicsBeginImageContext(size);
            [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
            UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            cell.image = [NSData dataWithData:UIImagePNGRepresentation(scaledImage)];

        }
        out = [outputView stringByEvaluatingJavaScriptFromString:@"isageGetOutputForStorage(true)"];
        out=[out stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        cell.output = out;
    }
    [delegate saveEditViewForBackground:self];
}

#pragma mark -
#pragma mark Button methods

- (void)newCell:(id)sender {
    // Popping the view controller means we lose track of the
    // navigation bar, so we pretend it's disapprearing.
    [self viewWillDisappear:NO];
    [self.navigationController popViewControllerAnimated:NO];
    [delegate editViewControllerDidFinish:self
                              nextDefault:@""
                               inLanguage:nil];
}

- (void)newCellWithDefault:(AlephCell*)initial_value {
    if ( ![[self.selectedObject valueForKey:@"input"] isEqual:[inputView text]] ) {
        [self.selectedObject setValue:[inputView text] forKey:@"input"];
        [self.selectedObject setValue:[NSNumber numberWithInt:AlephCellStatusHasInput] forKey:@"status"];
    }

    // Popping the view controller means we lose track of the
    // navigation bar, so we pretend it's disapprearing.
    [self viewWillDisappear:NO];
    [self.navigationController popViewControllerAnimated:NO];
    [delegate editViewControllerDidFinish:self
                              nextDefault:initial_value.input
                               inLanguage:initial_value.language];
}

- (IBAction)EvalShareButton:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex==0) {
        [self evalButtonPressed];
    } else if (sender.selectedSegmentIndex==1){
        [self shareButtonPressed];
    }
    sender.selectedSegmentIndex=UISegmentedControlNoSegment;
    
}

-(void) evalButtonPressed{
    self.Loaded=YES;
    [self eval:nil];
}

-(void) shareButtonPressed{
    [self shareCell:[[UIBarButtonItem alloc] init]];
}

- (void)eval:(id)sender {
    //NSLog(@"nonself: %@",[[selectedObject valueForKey:@"language"] lowercaseString]);
    [self.selectedObject setValue: [self.inputView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"input"];
    [self.selectedObject setValue:[NSNumber numberWithInt:AlephCellStatusHasInput] forKey:@"status"];
    if ( ![self isLoaded] ) {
        self.shouldLoad = YES;
    } else {
        [self.outputView stringByEvaluatingJavaScriptFromString:
         [NSString stringWithFormat:@"isageSetAndEval(%@,%@)",
          [self.inputView.text JSONString],
          [[[self.selectedObject valueForKey:@"language"] lowercaseString] JSONString]]];
    }
    //[self updateToolbarForEvaluation:NO];
    [self.inputView resignFirstResponder];
    
}



- (void)selectLanguage:(id)sender {

    BOOL isPhone = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
    if (!self.actionSheet) {
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Language"
                                                  delegate:self
                                         cancelButtonTitle:(isPhone ? @"Cancel" : nil)
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:
                       @"Sage",
                       @"GAP",
                       @"GP",
                       @"HTML",
                       @"Maxima",
                       @"Octave",
                       @"Python",
                       @"R",
                       @"Singular",
                       nil];
    }

    if ( isPhone ) {
        [self.navigationController.navigationBar removeGestureRecognizer:self.longPress];
        [self.actionSheet showFromToolbar:self.navigationController.toolbar];
    } else if ( [sender isKindOfClass:[UILongPressGestureRecognizer class]]
               && [sender state] == UIGestureRecognizerStateBegan ) {

        UIView *superview = [[sender view] superview];
        CGPoint p = [sender locationOfTouch:0 inView:superview];
        // We need to remove the recognizer after we have queried if
        // for view and superview.  We need to remove it or it
        // prevents autorotation.  I'm not sure why.
        [self.navigationController.navigationBar removeGestureRecognizer:self.longPress];
        [self.actionSheet showFromRect:CGRectMake(p.x, p.y, 1, 1)
                           inView:superview animated:YES];
    }
}

- (void)shareCell:(id)sender {
    [self saveOutput:@"output"];
    [[delegate shareButtonDelegate] displayShareSheet:self
                                              forCell:(AlephCell*)self.selectedObject
                                           fromWhence:sender];
}

-(void)didUpdateWithJSON:(NSNotification*)notification{
    // NSLog(@"didUpdateWithJSON edit view");
    [self updateOutput];
}

-(void)updateOutput {
    [self updateImageSize];
}

- (void)hideKeyboard:(id)sender {
    [inputView resignFirstResponder];
}

#pragma mark -
#pragma mark Gestures

- (void)swipe:(id)sender {
    if ( [inputView isFirstResponder] ) {
        NSRange selectedRange = inputView.selectedRange;
        if ( [sender direction] == UISwipeGestureRecognizerDirectionRight ) {
            if ( selectedRange.length == 0 ) {
                selectedRange.location += 1;
            } else {
                selectedRange.location += selectedRange.length;
                selectedRange.length = 0;
            }
        } else if ( [sender direction] == UISwipeGestureRecognizerDirectionLeft ) {
            if ( selectedRange.length == 0 ) {
                selectedRange.location -= 1;
            } else {
                selectedRange.length = 0;
            }
        } else if ( [sender direction] == UISwipeGestureRecognizerDirectionUp ) {
            UITextRange *textSelectedRange = [inputView selectedTextRange];
            CGRect rect = [inputView caretRectForPosition:textSelectedRange.start];
            UITextPosition *newPosition = [inputView closestPositionToPoint:CGPointMake(rect.origin.x, rect.origin.y - inputView.font.leading)];
            NSInteger newLocation = [inputView offsetFromPosition:inputView.beginningOfDocument
                                                 toPosition:newPosition];
            selectedRange.location = newLocation;
        } else if ([sender direction] == UISwipeGestureRecognizerDirectionDown ) {
            UITextRange *textSelectedRange = [inputView selectedTextRange];
            CGRect rect = [inputView caretRectForPosition:textSelectedRange.start];
            UITextPosition *newPosition = [inputView closestPositionToPoint:CGPointMake(rect.origin.x, rect.origin.y + inputView.font.leading)];
            NSInteger newLocation = [inputView offsetFromPosition:inputView.beginningOfDocument
                                                 toPosition:newPosition];
            selectedRange.location = newLocation;
        }
        inputView.selectedRange = selectedRange;
    }
}

- (void)doubleSwipe:(id)sender {
    if ( [inputView isFirstResponder] ) {
        NSRange selectedRange = inputView.selectedRange;
        selectedRange.length += 1;
        if ( [sender direction] == UISwipeGestureRecognizerDirectionLeft ) {
            selectedRange.location -= 1;
        }
        inputView.selectedRange = selectedRange;
    }
}

- (void)twoTaps:(id)sender {
    self.FullScreenOutput= ![self isFullScreenOutput];
    self.inputView.hidden=!self.inputView.isHidden;
    self.evaluateAndShareSegmentedController.hidden=!self.evaluateAndShareSegmentedController.isHidden;
    if ([self isFullScreenOutput]) {
        [inputView resignFirstResponder];
        outputView.frame = outputView.superview.frame;
    }
    [self updateImageSize];
}

#pragma mark -
#pragma mark Gesture Recognizer delegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark -
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)as clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // We have to add it back here
    [self.navigationController.navigationBar addGestureRecognizer:self.longPress];

    if (buttonIndex != [as cancelButtonIndex]) {
        NSString * title = [as buttonTitleAtIndex:buttonIndex];
        self.navigationItem.title = title;
        [self.selectedObject setValue:title forKey:@"language"];
        [self textViewDidChange:inputView]; // Change back to the Evaluate button
    }
}


#pragma mark -
#pragma mark Web view delegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{

    NSURL* url = [request URL];
    webView.scalesPageToFit=YES;

    if ( [[url scheme] isEqualToString:@"isage"] ) {

        //Do your action here
        if ( [[url host] isEqualToString:@"doneLoading"] ) {
            // [self saveOutput];
            self.Loaded= YES;
            if (self.shouldLoad) {
                [self eval:self];
            }
            self.shouldLoad = NO;
        } else if ( [[url host] isEqualToString:@"status"] ) {
            [self saveOutput:[[url path] substringFromIndex:1]]; // cut off leading '/'
        } else {
            NSLog(@"passed data from web view : %@",url);
        }
        return NO;
    }
    return YES;
}

#pragma mark -
#pragma mark Text view delegate methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView {

    // Load the nib for the accessory view
    if (aTextView.inputAccessoryView == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"keyboardAccessory" owner:self options:nil];
        // Loading the AccessoryView nib file sets the accessoryView outlet.
        aTextView.inputAccessoryView = keyboardAccessory;
    }

    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textview {
    self.activeField = textview;
}

- (void)textViewDidEndEditing:(UITextView *)textview {
    self.activeField = nil;
}

- (void)textViewDidChange:(UITextView *)textView {
    //[self updateToolbarForEvaluation:YES];
}

#pragma mark -
#pragma mark Accessory View

- (IBAction)insertDelimiters:(id)sender {
    // When the accessory view button is tapped, add a suitable string to the text view.
    NSMutableString *text = [inputView.text mutableCopy];
    NSRange selectedRange = inputView.selectedRange;

    NSString* delimiters = [sender title];
    [text insertString:[delimiters substringFromIndex:1] atIndex:selectedRange.location+selectedRange.length];
    [text insertString:[delimiters substringToIndex:1] atIndex:selectedRange.location];

    selectedRange.location += 1;
    inputView.text = text;
    inputView.selectedRange = selectedRange;
}

- (IBAction)insertTab:(id)sender {
    // When the accessory view button is tapped, add a suitable string to the text view.
    NSMutableString *text = [inputView.text mutableCopy];
    NSRange selectedRange = inputView.selectedRange;

    // Set text
    [text replaceCharactersInRange:selectedRange withString:@"    "];
    inputView.text = text;

    // Set cursor position
    selectedRange.location += 4;  // we put 4 spaces in -- we maybe want to change this to a tab
    selectedRange.length = 0;
    inputView.selectedRange = selectedRange;
}

- (IBAction)insertCharacter:(id)sender {
    // When the accessory view button is tapped, add a suitable string to the text view.
    NSMutableString *text = [inputView.text mutableCopy];
    NSRange selectedRange = inputView.selectedRange;

    NSString * ins = [sender title];
    if ( selectedRange.length == 0 && selectedRange.location > 0
        && [ins isEqualToString:@">"]
        && [text characterAtIndex:selectedRange.location-1 ] == '>' )
    {
        selectedRange.location -= 1;
        selectedRange.length += 1;
        ins = @"<";
    }

    // Set text
    [text replaceCharactersInRange:selectedRange withString:ins];
    inputView.text = text;

    // Set cursor position
    selectedRange.location += 1;
    selectedRange.length = 0;
    inputView.selectedRange = selectedRange;
}


#pragma mark -
#pragma mark Keyboard management

// for EditViewController protocol
- (void)registerForKeyboardNotifications
{
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
}


- (void)dealloc {
    if ( [self.navigationItem respondsToSelector:@selector(setRightBarButtonItems:)] ) {
        self.navigationItem.rightBarButtonItems = nil;
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
    [outputView setDelegate:nil];

}

#pragma mark -
#pragma mark View rotation


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self updateImageSize];
    [outputView stringByEvaluatingJavaScriptFromString:@"updateWidth()"];
}


- (void)updateImageSize {
    // Make the input area unobscured by the keyboard
    // The image view doesn't resize well, so use the outputView's size instead
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if ( UIInterfaceOrientationIsLandscape(self.interfaceOrientation) ) {
            inputView.frame = CGRectMake( 5, 5, inputView.superview.frame.size.width-10, 67 - 11 );
            //outputView.frame = outputView.frame;
        } else {
            inputView.frame = CGRectMake( 5, 5, 310, 105 );
            //outputView.frame = outputView.frame;
        }
    }
    if ( ![self isFullScreenOutput] ) {
        int yoffset = inputView.frame.size.height+10;
        outputView.frame = CGRectMake( 0, yoffset,
                                      inputView.superview.frame.size.width,
                                      inputView.superview.frame.size.height - yoffset);
    }
}

#pragma mark - UISplitViewControllerDelegate
- (void) awakeFromNib
{
    self.splitViewController.delegate=self;
}

- (BOOL) splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}

- (void) splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    //barButtonItem.title = aViewController.title;
    barButtonItem.title = @"Cells";
    self.navigationItem.leftBarButtonItem=barButtonItem;
}

- (void) splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationItem.leftBarButtonItem=nil;
}

-(void)selectedEquation:(NSManagedObject *)newEquation
{
    [self setSelectedObject:newEquation];
}










@end
