//
//  AlephCell.m
//  iSage
//
//  Created by Ivan Andrus on 4/29/11.
//  Copyright 2011 Ivan Andrus. All rights reserved.
//
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

#import "AlephCell.h"
#import "URLutils.h"
#import "JSON+iSage.h"

@implementation AlephCell

@dynamic status;
@dynamic alephid;
@dynamic creation_time;
@dynamic input;
@dynamic output;
@dynamic html_output;
@dynamic language;
@dynamic image;

@synthesize numRequests;
@synthesize isWorking;


- (void)alloc {
    isWorking = NO;
}

- (NSString*)alephURL{
    NSString* alephURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"sageServer"];
    if ( [alephURL isEqual:@"CustomURL"] ) {
        alephURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"customSageServer"];
    }
    return alephURL;
}

- (BOOL)canDisplayOutput{

    switch ( [self.status intValue] ) {
        case AlephCellStatusHasOutput:
        case AlephCellStatusSyntaxError:
            return YES;

        case AlephCellStatusNetworkError:
        case AlephCellStatusJSONParseError:
        case AlephCellStatusUninitialized:
        case AlephCellStatusHasInput:
        case AlephCellStatusHasAlephID:
        default:
            return NO;
    }
}

// TODO: should make the caller control isWorking instead of forcing them to make the request
- (NSDictionary*)needsRequest:(BOOL)retryErrors {
    NSLog(@"needsRequest");
    // TODO: reset errors here

    // The only time it's safe to make another request over the top of the first
    const int status = [self.status intValue];
    if ( isWorking && status != AlephCellStatusHasAlephID && !retryErrors ) {
        return nil;
    }

    switch ( status ) {

        case AlephCellStatusNetworkError:
        case AlephCellStatusJSONParseError:
            // Handle Error cases
            if ( !retryErrors ) {
                isWorking = NO;
                return nil;
            }
            // Fall through to the computation request

        case AlephCellStatusHasInput:
            // Request an id
            // NSLog(@"request id for:%@",self.input);
            isWorking = YES;
            numRequests++;
            self.alephid = [NSString createUUID];
            return [NSDictionary dictionaryWithObjectsAndKeys:
                    [NSURL URLWithString:[NSString stringWithFormat:@"%@eval",[self alephURL]]], @"URL",
                    [NSDictionary dictionaryWithObjectsAndKeys:
                     self.alephid, @"session_id",
                     [NSString createUUID], @"msg_id",
                     @"true", @"sage_mode",
                     [self.input JSONString], @"commands",
                     nil], @"post_data",
                    nil];

        case AlephCellStatusHasAlephID:
            // Request status
            // NSLog(@"working on %@",self.alephid);
            isWorking = YES;
            numRequests++;
            return [NSDictionary dictionaryWithObject:
                    [NSURL URLWithString:[NSString stringWithFormat:@"%@output_poll?computation_id=%@",
                                          [self alephURL], self.alephid]]
                                               forKey:@"URL"];

        case AlephCellStatusHasOutput:
        case AlephCellStatusSyntaxError:
        case AlephCellStatusUninitialized:
        default:
            isWorking = NO;
            return nil;
    }
    return nil;
}


- (void)clearError{
    isWorking = NO;
    numRequests = 0;
    if ( self.input ) {
        self.status = [NSNumber numberWithInt:AlephCellStatusHasInput];
    } else {
        self.status = [NSNumber numberWithInt:AlephCellStatusUninitialized];
    }
}

- (void)setError:(BOOL)wasNetworkError{
    isWorking = NO;
    self.status = [NSNumber numberWithInt:
                   (wasNetworkError ? AlephCellStatusNetworkError : AlephCellStatusJSONParseError)];
}

// Return YES if should re-request this
- (BOOL)updateWithResponse:(id)response{
    NSLog(@"updateWithResponse");
    isWorking = NO;
    // NSLog(@"updateWithResponse: %@",response);

    // Fake update
    switch ( [self.status intValue] ) {

        case AlephCellStatusNetworkError:
        case AlephCellStatusJSONParseError:
            NSLog(@"Something is rotten in Denmark");
            break;

        case AlephCellStatusHasInput:
            // getting an id
            // NSLog(@"Got an id: %@ %@",[response class], response);
            if ( [response isKindOfClass:[NSString class]] ) {
                // self.alephid = (NSString*)response;
                self.status = [NSNumber numberWithInt:AlephCellStatusHasAlephID];
            } else if ([response isKindOfClass:[NSDictionary class]] &&
                       [[response objectForKey:@"session_id"] isKindOfClass:[NSString class]])
            {
                self.alephid = (NSString*)[response objectForKey:@"session_id"];
                self.status = [NSNumber numberWithInt:AlephCellStatusHasAlephID];
                NSLog(@"New style session_id: %@",self.alephid);

            } else {
                NSLog(@"This is a problem: %@",response);
                self.status = [NSNumber numberWithInt:AlephCellStatusJSONParseError];
            }
            numRequests = 0;
            break;

        case AlephCellStatusHasAlephID: {

            if ( ! [response isKindOfClass:[NSDictionary class]] ) {
                self.status = [NSNumber numberWithInt:AlephCellStatusJSONParseError];
                break;
            }
            // NSLog(@"response: %@",response);
            NSArray * content = [response objectForKey:@"content"];
            if ( !content || ![content isKindOfClass:[NSArray class]] ) {
                // This means there is nothing to show.
                return YES;
            }

            // We have new output, so get rid of any old stuff we might have
            self.image = nil;
            // We add a newline before any output, so we will strip that off before returning
            self.output = @"";
            for (id resp in content) {

                if ( ![resp isKindOfClass:[NSDictionary class]] )  continue;

                NSDictionary * content = [resp objectForKey:@"content"];
                if ( ![content isKindOfClass:[NSDictionary class]] ) continue;

                NSString * messageType = [resp objectForKey:@"msg_type"];
                // TODO: factor out as much as I can
                if ( [messageType isEqual:@"extension"] ) {

                    // End of a session so we don't need to make any more requests
                    if ( [[content objectForKey:@"msg_type"] isEqualToString:@"session_end"] ) {
                        // If we haven't set the status to something else, set it to success
                        if ( [self.status isEqualToNumber:[NSNumber numberWithInt:AlephCellStatusHasAlephID]] ) {
                            self.status = [NSNumber numberWithInt:AlephCellStatusHasOutput];
                        }
                        if ([self.output length] > 0) {
                            self.output = [self.output substringFromIndex:1];
                        }
                        return NO;
                    }

                } else if ( [messageType isEqual:@"pyout"] ) {

                    id data = [content objectForKey:@"data"];
                    if ( [data isKindOfClass:[NSDictionary class]] ) {
                        self.output = [self.output stringByAppendingFormat:@"\n%@",
                                       [data objectForKey:@"text/plain"]];
                    } else if ( [data isKindOfClass:[NSString class]] ) {
                        self.output = [self.output stringByAppendingFormat:@"\n%@", data];
                    } else {
                        self.output = [self.output stringByAppendingString:@"\nUnable to parse output"];
                    }

                } else if ( [messageType isEqual:@"stream"] ) {
                    if ( [[content objectForKey:@"name"] isEqualToString:@"stdout"] ||
                         [[content objectForKey:@"name"] isEqualToString:@"stderr"] )
                    {
                        self.output = [self.output stringByAppendingFormat:@"\n%@",
                                       [content objectForKey:@"data"]];
                    }

                } else if ( [messageType isEqual:@"execute_reply"] ) {

                    NSString * status = [content objectForKey:@"status"];
                    if ( [status isEqual:@"ok"] ) {
                        // Status will be set at end_session
                        // self.status = [NSNumber numberWithInt: AlephCellStatusHasOutput];

                    } else if ( [status isEqual:@"error"] ) {
                        self.status = [NSNumber numberWithInt: AlephCellStatusSyntaxError];
                        self.output = [self.output stringByAppendingFormat:@"\n%@:",
                                       [content objectForKey:@"evalue"]];

                        // Strip escape sequences and concatenate the traceback
                        id traceback = [content objectForKey:@"traceback"];
                        if ( [traceback isKindOfClass:[NSString class]] ) {
                            self.output = [self.output stringByAppendingFormat:@"\n%@",
                                           [traceback stripEscapeSequences]];
                        } else if ( [traceback isKindOfClass:[NSDictionary class]] ) {
                            for (NSString *frame in traceback) {
                                if ( [frame isKindOfClass:[NSString class]] ) {
                                    self.output = [self.output stringByAppendingFormat:@"\n%@",
                                                   [frame stripEscapeSequences]];
                                }
                            }
                        } else {
                            self.output = [self.output stringByAppendingString:@"\nUnable to parse exception"];
                        }
                    }

                } else if ( [messageType isEqual:@"display_data"] ) {

                    NSString * fileName = [[content objectForKey:@"data"] objectForKey:@"text/filename"];
                    if ( [fileName isEqual:@"sage0.png"] ) { // TODO: Horrible hack to only load the first and ensure it's a png

                        NSString * path = [NSString stringWithFormat:@"%@files/%@/%@",
                                           [self alephURL],
                                           self.alephid,
                                           fileName];
                        NSURL * imageURL = [NSURL URLWithString:path];
                        NSData * imgData = [NSData dataWithContentsOfURL:imageURL];
                        self.image = imgData;

                    } else {
                        NSLog(@"Ignoring file with name: %@",fileName);
                    }
                }
            }
            if ([self.output length] > 0) {
                self.output = [self.output substringFromIndex:1];
            }
            // We didn't get the whole response back so ask for more
            return YES;

        }
        case AlephCellStatusHasOutput:
            NSLog(@"This shouldn't happen either: %@",response);

        case AlephCellStatusSyntaxError:
        case AlephCellStatusUninitialized:
        default:
            // NSLog(@"nothing to request");
            isWorking = NO;
            numRequests = 0;
    }
    return NO;
}


@end
