Sage iPhone app
===============

This was converted to git from [a mercurial repository](https://bitbucket.org/gvol/sage-iphone-app/overview).
If something seems amiss, you can try checking there.

Introduction
------------

This iPhone app is a simple interface to the
[Sage computer algebra system](http://www.sagemath.org/).  The latest
released version can be found
[on the iOS app store](http://itunes.apple.com/us/app/sage-math/id496492945).

This is a lightweight alternative to the
[Sage online notebook](http://www.sagenb.org) Sage online notebook
designed for quick calculations.  Network connectivity is required to
perform calculations, but results are stored on the device for offline
perusal and can be easily emailed to colleagues.  Several buttons are
added to the keyboard to simplify text input.

Bugs and Feature Resquests
--------------------------

If you have ideas for improvements or wish to report a bug you may do so
[on bitbucket](https://bitbucket.org/gvol/sage-iphone-app/issues?status=new&status=open).
Not every idea I have for improvement is located there, but I plan to
move more and more there as time goes on.

License
-------

The Sage iPhone app uses the
[JSONKit library](https://github.com/johnezang/JSONKit) under the BSD
license and is itself released under the same license.

Building
--------

If you want to work on the Sage iPhone app you will need access to
Xcode.  Check out the source code or download an archive file and
unpack it.  Open `iSage.xcodeproj` and then build, it should download
the latest version of JSONKit automatically.
